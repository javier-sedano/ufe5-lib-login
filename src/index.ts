export class Fact {

    public factorial(n: number): number {
        if (n<=1) {
            return 1;
        } else {
            return n * this.factorial(n-1);
        }
    }
}

export const fact = new Fact();

export * from './OidcService';
