import { fact } from '../src/index';

describe("factorial()", () => {
    test("Should return 1 for a negative number", () => {
        expect(fact.factorial(-1)).toEqual(1);
    });
    test("Should return 1 for 0", () => {
        expect(fact.factorial(0)).toEqual(1);
    });
    test("Should return 1 for 1", () => {
        expect(fact.factorial(1)).toEqual(1);
    });
    test("Should return 2 for 2", () => {
        expect(fact.factorial(2)).toEqual(2);
    });
    test("Should return 6 for 3", () => {
        expect(fact.factorial(3)).toEqual(6);
    });
    test("Should return 120 for 5", () => {
        expect(fact.factorial(5)).toEqual(120);
    });
});
