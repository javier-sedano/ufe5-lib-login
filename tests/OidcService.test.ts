import { jwt2String } from '../src/OidcService';

describe("jwt2String()", () => {
    test("Should return empty for undefined", () => {
        expect(jwt2String(undefined)).toEqual('');
    });
    test("Should return the description of an encoded JWT", () => {
        const jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0YWciOiJ2YWx1ZSIsImlhdCI6MTY5OTAzMjY2MX0.gunTOgbVwyfsZRLeeoSzt5vfg6F7Z0L93x_JD44B_hU';
        const expectedHeader = {
            alg: 'HS256',
            typ: 'JWT'
        };
        const expectedBody = {
            tag: 'value',
            iat: 1699032661
        };
        const expectedString = JSON.stringify(expectedHeader, null, 2)
            + '\n'
            + JSON.stringify(expectedBody, null, 2);
        expect(jwt2String(jwt)).toEqual(expectedString);
    });
    test("Should return error for a wrong encoded JWT", () => {
        const jwt = 'notAToken';
        const expectedString = 'Not a JWT token?';
        expect(jwt2String(jwt)).toEqual(expectedString);
    });
});

describe("oidcService", () => {
    test("Should return empty for undefined", () => {
        console.log("I will not make more tests, this is not relevant in this demo");
    });
});